provider "discord" {
  token = var.discord_token
}

terraform {
  required_providers {
    discord = {
      source = "aequasi/discord"
    }
  }
}

