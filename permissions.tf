
data "discord_permission" "admin" {
  allow_extends    = data.discord_permission.moderator.allow_bits
  deny_extends     = data.discord_permission.moderator.deny_bits
  kick_members     = "allow"
  ban_members      = "allow"
  manage_nicknames = "allow"
  view_audit_log   = "allow"
  priority_speaker = "allow"
}

data "discord_permission" "moderator" {
  allow_extends    = data.discord_permission.member.allow_bits
  deny_extends     = data.discord_permission.member.deny_bits
  kick_members     = "allow"
  ban_members      = "allow"
  manage_nicknames = "allow"
  view_audit_log   = "allow"
  priority_speaker = "allow"
}

data "discord_permission" "member" {
  view_channel     = "allow"
  send_messages    = "allow"
  use_vad          = "deny"
  priority_speaker = "deny"
}

data "discord_permission" "everyone" {
  view_channel         = "allow"
  send_messages        = "allow"
  use_vad              = "deny"
  priority_speaker     = "deny"
  view_audit_log       = "allow"
  read_message_history = "allow"
}

