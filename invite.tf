resource "discord_invite" "link" {
  channel_id = discord_text_channel.general.id
  max_age    = 0
}

output "discord_invite_link" {
  value = discord_invite.link.id
}