resource "discord_role" "admin" {
  server_id   = discord_server.coinfinder_server.id
  name        = "Admin"
  permissions = data.discord_permission.moderator.allow_bits
  color       = data.discord_color.red.dec
  hoist       = true
  mentionable = true
  position    = 3
}

resource "discord_role" "moderator" {
  server_id   = discord_server.coinfinder_server.id
  name        = "Moderator"
  permissions = data.discord_permission.moderator.allow_bits
  color       = data.discord_color.blue.dec
  hoist       = true
  mentionable = true
  position    = 2
}

resource "discord_role" "member" {
  server_id   = discord_server.coinfinder_server.id
  name        = "Member"
  permissions = data.discord_permission.member.allow_bits
  color       = data.discord_color.green.dec
  hoist       = true
  mentionable = true
  position    = 1
}


resource "discord_role_everyone" "everyone" {
  server_id   = discord_server.coinfinder_server.id
  permissions = data.discord_permission.everyone.allow_bits
}

