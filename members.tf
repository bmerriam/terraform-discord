resource "discord_member_roles" "malakai" {
  user_id   = var.malakai_user_id
  server_id = discord_server.coinfinder_server.id
  role {
    role_id = discord_role.admin.id
  }
}