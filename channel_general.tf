resource "discord_text_channel" "general" {
  name      = "general"
  server_id = discord_server.coinfinder_server.id
  position  = 0
  category  = discord_category_channel.chatting.id
}


resource "discord_channel_permission" "general_mod" {
  type         = "role"
  overwrite_id = discord_role.moderator.id
  allow        = data.discord_permission.moderator.allow_bits
  deny         = data.discord_permission.moderator.deny_bits
  channel_id   = discord_text_channel.general.id
  #  position     = 0
}

resource "discord_channel_permission" "general_member" {
  type         = "role"
  overwrite_id = discord_role.member.id
  allow        = data.discord_permission.member.allow_bits
  deny         = data.discord_permission.member.deny_bits
  channel_id   = discord_text_channel.general.id
  #  position     = 1
}