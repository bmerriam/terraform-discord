data "discord_color" "blue" {
  hex = "#4287f5"
}

data "discord_color" "red" {
  hex = "#cc0000"
}

data "discord_color" "green" {
  hex = "#638600"
}
 